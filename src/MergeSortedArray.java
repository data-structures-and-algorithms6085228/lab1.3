public class MergeSortedArray {
    public static void printArray(int[] arr){
        System.out.print("Merged Array: ");
        System.out.print('[');
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if(i < arr.length-1){
                System.out.print(',');
            }
        }
        System.out.println(']');

    }

    public static void main(String[] args) {
        int[] nums1 = {1,2,3,0,0,0};
        int[] nums2 = {2,5,6};
        int m = 3;
        int n = 3;
        merge(nums1, m, nums2, n);
        printArray(nums1);
        //--------------------------------
        nums1 = new int[] {1};
        nums2 = new int[] {};
        m = 1;
        n = 0;
        merge(nums1, m, nums2, n);
        printArray(nums1);
        //--------------------------------
        nums1 = new int[] {0};
        nums2 = new int[] {1};
        m = 0;
        n = 1;
        merge(nums1, m, nums2, n);
        printArray(nums1);
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1;
        int j = n - 1;
        int k = m + n - 1;
        
        while (j >= 0) {
            if (i >= 0 && nums1[i] > nums2[j]) {
                nums1[k--] = nums1[i--];
            } else {
                nums1[k--] = nums2[j--];
            }
        }
    }
}
